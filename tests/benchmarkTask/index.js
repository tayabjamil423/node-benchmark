const axios = require("axios").default;

const { benchmarkTask } = require("../..");

const taskFunc = () => axios.get("https://www.google.com/").catch((err) => {});

benchmarkTask({
	logNamespace: "test",
	name: "dummy",
	taskFunc: taskFunc,
	maxTaskCount: 10,
	maxConcurrency: 2,
	logOnTaskComplete: true
});
