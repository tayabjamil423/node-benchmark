const debug = require("debug");
const lodash = require("lodash");
const now = require("performance-now");

const defaultParams = {
	logNamespace: null,
	name: null,
	taskFunc: null,
	maxTaskCount: 100,
	maxConcurrency: 4,
	logOnTaskComplete: true,
};

const benchmarkTask = async (params) => {
	if (!params) {
		throw new Error("Param 'params' is required.");
	}

	params = lodash.defaults(params, defaultParams);

	const {
		logNamespace,
		name,
		taskFunc,
		maxTaskCount,
		maxConcurrency,
		logOnTaskComplete,
	} = params;

	if (!logNamespace) {
		throw new Error("Field 'params.logNamespace' is required.");
	}

	if (!name) {
		throw new Error("Field 'params.name' is required.");
	}

	if (!taskFunc) {
		throw new Error("Field 'params.taskFunc' is required.");
	}

	const log = debug(logNamespace);
	log.enabled = true;

	let ranTaskCount = 0;
	let runningTaskCount = 0;
	const runningTasks = [];

	let totalElapsedMs = 0;
	let avgElapsedMs = null;

	log(
		"Running benchmark: %s - Max Task Count: %d, Max Concurrency: %d",
		name,
		maxTaskCount,
		maxConcurrency
	);

	for (let taskNum = 1; taskNum <= maxTaskCount; taskNum++) {
		if (runningTaskCount < maxConcurrency) {
			const startTimeMs = now();
			const task = taskFunc().then(() => {
				const endTimeMs = now();
				const elapsedTimeMs = Math.floor(endTimeMs - startTimeMs);

				ranTaskCount++;
				totalElapsedMs += elapsedTimeMs;
				avgElapsedMs = Math.floor(totalElapsedMs / ranTaskCount);

				if (logOnTaskComplete) {
					log(
						"Task %d completed - Elapsed Time: %d, Total: %dms, Avg: %dms",
						taskNum,
						elapsedTimeMs,
						totalElapsedMs,
						avgElapsedMs
					);
				}
			});
			runningTasks.push(task);
			runningTaskCount++;
		} else {
			await Promise.race(runningTasks);
			runningTaskCount--;
			runningTasks.shift();
			taskNum--;
		}
	}
	await Promise.all(runningTasks);

	log(
		"🟢 All tasks completed - Total Elasped Time: %dms, Avg: %dms",
		totalElapsedMs,
		avgElapsedMs
	);
};

module.exports = benchmarkTask;
